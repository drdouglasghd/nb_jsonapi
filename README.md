# Super Simple JSON Magento API Wrapper #

### What is this repository for? ###

* Allows Simple HTTP JSON interactions with Magento
* Version: 0.0.1

### Usage ###

Endpoint POST
```
https://your.magento.domain/json/api/call/
```

Example JSON Payload
```
#!json

{
	"user": "webservice-username",
	"token": "webservice-token",
	"resource_path": "product.list",
	"args": [{
		"sku": {
			"like": "%mysku%"
		}
	}]
}

```

Expect Results
```
#!json

{
	"results": [{
		"product_id": "1003",
		"sku": "mysku",
		"name": "My Product",
		"set": "64",
		"type": "configurable",
		"category_ids": []
	}]
}

```

Create a Product - JSON Payload
```
#!json
{
    "user": "webservice-username",
    "token": "webservice-token",
    "resource_path": "product.create",
    "args": [
        "simple",
        "4",
        "SKU",
        {
            "categories": [
                2
            ],
            "websites": [
                1
            ],
            "name": "ProductName",
            "description": "Productdescription",
            "short_description": "Productshortdescription",
            "weight": "10",
            "status": "1",
            "visibility": "4",
            "price": "100",
            "tax_class_id": "1"
        }
    ]
}
```
Expect Results - Product ID
```
#!json

{"results":"2"}

```

### How do I get set up? ###

* Configuration
* * Installation in Magento Store is only configuration 

* Dependencies
* * Mage_Api

* Common Pitfalls
* * The "args" needs to be a json array, with the arguments each in sequence. These are passed into the API engine in that sequence.
* * If you are creating the HTTP Request in PHP, then you need to be mindful of how the json_encode() converts PHP Arrays into JSON Arrays and Objects.
* * * $json_array = array('list','of','things');
* * * $json_object = array('property'=>'value');

### Who do I talk to? ###

* drdouglasghd | geoff@neverbehind.com